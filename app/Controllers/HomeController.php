<?php

namespace App\Controllers;

use Core\Controller;
use Core\View;

class HomeController extends Controller
{
    protected function index()
    {
        View::render('home/index');
    }
}
