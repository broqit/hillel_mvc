<?php
define('SITE_URL', 'http://ithillel-mvc.tech:8890');
define('BASE_DIR', dirname(__DIR__));
define('CONTROLLERS_NAMESPACE', 'App\Controllers\\');
define('ASSETS_URI', 'assets/');

/**
 * DB constants
 */
define('DB_HOST', '127.0.0.1');
define('DB_NAME', 'hillel_mvc');
define('DB_USER', 'root');
define('DB_PASSWORD', 'root');
